# Hello Special [![by kuus](https://img.shields.io/badge/by-kuus-lightgrey.svg?style=social)]()

[![GitHub release](https://img.shields.io/gitlab/release/kuus/hellospecial.svg)]()
[![Build Status](https://scrutinizer-ci.com/g/kuus/hellospecial/badges/build.png?b=master)](https://scrutinizer-ci.com/g/kuus/hellospecial/build-status/master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/gp/hellospecial/badges/quality-score.png?b=master&s=550d9409e7fd819da23abcd1ea0d1d967b87c0e3)](https://scrutinizer-ci.com/gp/hellospecial/?branch=master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/7244d24b48ab4cdd95425136844407d6)](https://www.codacy.com?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=kuus/hellospecial&amp;utm_campaign=Badge_Grade)
[![devDependency Status](https://david-dm.org/kuus/hellospecial/dev-status.svg)](https://david-dm.org/kuus/hellospecial#info=devDependencies)

> Hello Special website.


Dev Info
---------------
- `npm run serve` (or `npm run s`)
- `npm run build` (or `npm run b`)
- `npm run watch` (or `npm run w`)

Useful command:
`npm run b && git add . --all && git commit -am "rebuild static files" && git push && npm run d`


Bugs
---------------
If you find an issue, please let us know [here](https://gitlab.com/kuus/hellospecial/issues?state=open), thanks!


License
---------------
 [![License](https://img.shields.io/badge/license-MIT-blue.svg)](http://doge.mit-license.org) [![kuus](https://img.shields.io/badge/%C2%A9kuus-2017-blue.svg)](https://pluswp.com)


---------------
kuus 2017 | [kunderikuus.net](http://kunderikuus.net) | kunderikuus@gmail.com
