/*!
 * Heavily inspired by
 *
 * SVG Pie Timer 0.9.1 | Anders Grimsrud, grint.no | MIT License | github.com/agrimsrud/svgPieTimer.js
 */
export default class SvgPieTimer {

  constructor (props) {
    var self = this;
    this._element = props.element;
    this._size = 40;
    this._startFrom = props.startFrom ? props.startFrom * 1000: 0; // startFrom is given in seconds
    this._duration = props.duration ? props.duration * 1000 : 1000; // duration is given in seconds
    this._actualDuration = this._duration - this._startFrom;
    var n = props.loops;

    if (this.req) {
      this.stop();
    }

    // This part might be confusing:
    // If n==0, do infinite loops
    // In other cases where n is set, do n loops
    // If n is not set, do 1 loop
    // Do it this way to prevent mixing n==0 and !n
    n = (n === 0) ? 0 : n ? n : 1;

    var end = Date.now() + this._actualDuration * n;
    var totaldur = this._actualDuration * n;

    // Date.now fix by Ari Fuchs, afuchs.tumblr.com/post/23550124774/date-now-in-ie8
    Date.now = Date.now || function() { return +new Date(); };

    // Animate frame by frame
    (function frame() {
      var current = Date.now();
      var remaining = end - current;

      // Now set rotation rate
      // E.g. 50% of first loop returns 1.5
      // E.g. 75% of sixth loop returns 6.75
      // Has to return >0 for SVG to be drawn correctly
      // If you need the current loop, use Math.floor(rate)
      var rate = n + 1 - remaining / self._duration;
      // total rate for a full cycle is 0 to 2
      rate = rate;// + ((self._startFrom * 2) / self._duration);

      // As requestAnimationFrame will draw whenever capable,
      // the animation might end before it reaches 100%.
      // Let's simulate completeness on the last visual
      // frame of the loop, regardless of actual progress
      // console.log(current)
      if (remaining < 60) {
        // 1.0 might break, set to slightly lower than 1
        // Update: Set to slightly lower than n instead
        self._draw(n - 0.0001);
        // Stop animating when we reach n loops (if n is set)
        if(remaining < totaldur && n) {
          return;
        }
      }
      // To reverse, uncomment this line
      // rate = 360 - rate;

      // draw
      self._draw(rate);
      // request next frame
      self.req = requestAnimationFrame(frame);
    }());
  }

  /**
   * Draw SVG path
   *
   * x on the right side is positive, y on the bottom side is positive,
   * on the other sides they are negative
   * @param  {Number} rate
   */
  _draw (rate) {
    let angle = 360 * rate;
    angle %= 360;
    // if (angle >= 359.9) {
    //   console.log(angle)
    // }

    let rad = (angle * Math.PI / 180);
    let mid = (angle > 180) ? 1 : 0;
    let x = Math.sin(rad) * this._size;
    let y = Math.cos(rad) * -this._size;
    let shape = `M 0 0 v -${this._size} A ${this._size} ${this._size} 1 ${mid} 1 ${x} ${y} z`;

    this._element.setAttribute('d', shape);
  }

  /**
   * Stop the animation cancel the request animation frame
   */
  stop () {
    window.cancelAnimationFrame(this._req);
  }

  /**
   * Destroy the animation
   * @return {[type]} [description]
   */
  destroy () {
    this.stop();
  }
}
