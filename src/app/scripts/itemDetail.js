import $ from 'jquery';
import api from './api';
// import 'slick-carousel/slick/slick.js';
// import 'sticky-kit/dist/sticky-kit';
// import 'sticky-js/dist/sticky.min';

class ItemDetail {

  constructor () {
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
  }

  /**
   * On document ready
   */
  $onReady () {
    this.__$thumbs = $('#appItemDetail__thumbs');
    this.__$slider = $('#appItemDetail__slider');
    this.__$sticky = $('#appItemDetail__main-sticky');

    if (this.__$slider.length && this.__$thumbs.length) {
      this._initSliderThumbs();
      this._initSlider();
      $(window).on('resize', this._onResizeSliders.bind(this));
    }

    if (this.__$sticky.length) {
      // this.__$sticky.stick_in_parent();
      this._initSticky();
      $(window).on('resize', this._onResizeSticky.bind(this));
    }
  }

  /**
   * On resize slides behaviours
   */
  _onResizeSliders () {
    // no slider if there is only one image
    if (this.__$slider.find('img').length === 1) {
      this.__$slider.slick('unslick');
      this.__$thumbs.slick('unslick');
    } else {

    }
  }

  /**
   * On resize sticky behavious
   */
  _onResizeSticky () {
    const screenLgVersion = $(window).width() >= 992; // @@ref $grid-breakpoints: lg,

    if (this._stickyInitialised && screenLgVersion) {
      this.__$sticky.unstick();
      this._stickyInitialised = false;
    } else {
      this._initSticky();
    }
  }

  /**
   * Init sticky
   */
  _initSticky () {
    if (this._stickyInitialised) {
      return;
    }
    this.__$sticky.sticky({
      responsiveWidth: true,
      className: 'is-sticky'
    });
    // this._sticky = new Sticky('#appItemDetail__main-sticky');
    this._stickyInitialised = true;
  }

  /**
   * Init slider thumbnails
   */
  _initSliderThumbs () {
    if (!this.__$thumbs.length || this._sliderThumbsInitialised) {
      return;
    }

    this.__$thumbs.slick({
      vertical: true,
      // verticalSwiping: true,
      // centerMode: screenLgVersion,
      slidesToShow: 8,
      slidesToScroll: 1,
      asNavFor: this.__$slider,
      focusOnSelect: true,
      arrows: false,
      // infinite: false,
      responsive: [{
        breakpoint: 992, // @@ref $grid-breakpoints: lg
        settings: {
          vertical: false,
          slidesToShow: 5,
        }
      }]
    });

    this._sliderThumbsInitialised = true;

    // remove active class from all thumbnail slides
    this.__$thumbs.find('.slick-slide').removeClass('slick-active');

    // set active class to first thumbnail slides
    this.__$thumbs.find('.slick-slide').eq(0).addClass('slick-active');
  }

  /**
   * Init slider
   */
  _initSlider () {
    if (!this.__$slider.length || !this.__$thumbs.length || this._sliderInitialised) {
      return;
    }

    this.__$slider.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      arrows: true,
      asNavFor: this.__$thumbs
    });

    this._sliderInitialised = true;

    // on before slide change match active thumbnail to current slide
    this.__$slider.on('beforeChange', (event, slick, currentSlide, nextSlide) => {
      this.__$thumbs.find('.slick-slide').removeClass('slick-active');
      this.__$thumbs.find('.slick-slide').eq(nextSlide).addClass('slick-active');
    });
  }

  /**
   * Reinit UI
   */
  reinitUI () {
    this._initSlider();
    this._initSliderThumbs();
    this._initSticky();
  }
}

// export to public API
api['itemDetail'] = new ItemDetail();

export default ItemDetail;
