import $ from 'jquery';
import api from './api';

class Seller {

  constructor () {
    // bootstrap
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * On document ready
   */
  $onReady () {
    // console.log('app.js document is ready');
    this.__$wrapper = $('#hsSellercontact');

    // bail if we are not on the right page
    if (!this.__$wrapper.length) {
      return;
    }

    this.__$countrySelector = $('.hsLeverenCountry');

    this._bindUI();
  }

  /**
   * Bind UI
   */
  _bindUI () {
    const self = this;

    self.__$countrySelector.hide();
    // if (!!$('input[name="leveren"]:checked').val()) {
    //   self.__$countrySelector.slideDown();
    // }

    $('input[name="leveren"]').on('change', function () {
      const isYes = this.value === 'ja';
      if (isYes) {
        self.__$countrySelector.slideDown();
      } else {
        self.__$countrySelector.slideUp();
      }
    });
  }
}

// export to public API
api['seller'] = new Seller();

export default Seller;
