import $ from 'jquery';
import api from './api';

class ABtest {

  constructor () {
    // bootstrap
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    // console.log('app.js initialized');
    this.tests = {
      'Standoutregistration': {},
    };
  }

  /**
   * On document ready
   */
  $onReady () {
    // this.ABtest01();
  }

  /**
   * Run test
   * @param  {String} id
   */
  run (id) {
    if (this.tests[id]) {
      this['test' + id]();
    } else {
      console.error(`ABtest with id ${id} does not exists`);
    }
  }

  /**
   * Test 01: ABtest--standoutregistration
   */
  testStandoutregistration () {
    const $homepageCard = $('#homepageRegisterCard');
    const $body = $('body');
    if (!$homepageCard.length) {
      return;
    }
    $body.addClass('ABtest--standoutregistration');
    // on click anywhere remove the class
    document.body.addEventListener('click', () => {
      $body.removeClass('ABtest--standoutregistration');
    }, true);
  }
}

// export to public API
api['ABtest'] = new ABtest();
