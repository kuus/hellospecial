import api from './api';
import defaultStates from './states.defaults';
import defaultColors from './colors.defaults';
import SvgPieTimer from './svgPieTimer';

// Example use of the API
// (function (window, document, $, api) {
//   var myDOMorJquery = $('.hsItem')[0];

//   var myItem = new api.Item({
//     id: 10,
//     elem: myDOMorJquery,
//     // state: 3,
//     onState: function (item) {
//       console.log('onState ' + item.state.id, item);
//     },
//     onEnd: function (item) {
//       console.log('Auction ends! onEnd', item);
//     },
//     onBid: function (item) {
//       console.log('Bid! on item', item);
//     }
//   });
//   myItem.setState(0, 15);
//   // myItem.destroy();
// })(window, document, jQuery, window.hellospecial);

// (function(window, document, $, api) {
//   // calling the contructor or the setState method return the item instance
//   var myItem = new hellospecial.Item({
//     id: 12,
//     elem: $('.hsItem')[0] // the item element in the category page
//   }).setState('geboden', 11);
//   // grab the state in a variable to use closure
//   var myState = myItem.state;

//   // fake async loading of ajax content
//   setTimeout(function () {
//     var myItemPopup = new hellospecial.Item({
//       id: 12,
//       elem: $('.hsItem')[1] // the item element in the popup
//     });
//     myItemPopup.resumeState(myState);
//   }, 3000);

//   // fake async something elese
//   setTimeout(function () {
//     var myAsyncItem = new hellospecial.Item({
//       id: 12,
//       elem: $('.hsItem')[2] // another item element
//     });
//     myAsyncItem.resumeState(myState);
//   }, 5000);
// })(window, document, jQuery, window['hellospecial']);

/**
 * The Item class default options
 * @type {Object}
 */
const defaultOpts = {
  /** @type {Array} Default states */
  states: defaultStates,
  /** @type {string} attribute name */
  attrState: 'data-state',
  /** @type {string} attribute name */
  attrExpiration: 'data-expiration',
  /** @type {function(Item)} Callback at the end of each state */
  onState: function () {},
  /** @type {function(Item, Number)} Callback each second of a state progression */
  // onSecond: function () {},
  /** @type {function(Item)} Callback at the end of the last state */
  onEnd: function () {},
  /** @type {function(Item)} Callback at bid click */
  onBid: function () {},
}

/**
 * The Item class needs to be initialized on document ready with an id and a
 * container element
 */
class Item {

  /**
   * Constructor
   * @param  {Object} opts Contains properties:
   *                       'id': {string},
   *                       'elem': {HTMLelement},
   *                       'state': {?Number}
   * @return {Item}        The item instance
   */
  constructor (opts) {
    if (typeof opts.id === undefined) {
      throw new Error('`elem` property is required when instantiate a new Item');
      return;
    }
    if (typeof opts.id === undefined) {
      throw new Error('`id` property is required when instantiate a new Item');
      return;
    }

    /** @type {Object} Item given opts */
    this.opts = $.extend({}, defaultOpts, opts);

    /** @deprecated  @type {HTMLelement} Basic DOM container for the item */
    this.container = opts.elem.jquery ? opts.elem[0] : opts.elem;

    /** @type {jQuery} Basic DOM container for the item (jQuery wrap) */
    this.$container = opts.elem.jquery ? opts.elem : $(opts.elem);

    /** @type {string} Item id */
    this.id = opts.id || this.$container.attr('id');

    /** @type {Array} Contains the possible states for the item, as an array */
    this.states = this.opts.states;

    /** @type {Object} Contains the possible states for the item, as an object (indexed by id) */
    this.statesMap = this._getObjFromArray(this.opts.states, 'id');

    /** @type {function()} Holds the intervalled reference for one cycle */
    this._cycle = null;

    /** @type {Number} ID of one of the four states */
    this._initialStateId = opts.state || this.$container.attr(this.opts.attrState);

    /** @type {Array<SvgPieTimer>} One animation per svg element */
    this._animations = [];

    // set DOM related stuff
    this._bindUI();

    // if (this._initialStateId) {
    //   this.setState(this._initialStateId);
    // }

    // console.log('Item->constructor()', this);

    return this;
  }

  /**
   * Init
   */
  _bindUI () {

    /** @type {jQuery} The 'Bied' buttons */
    this.__$bid = this.$container.find('[data-bid]');

    /** @type {jQuery} */
    this.__$price = this.$container.find('[data-price]');

    /** @type {jQuery} */
    this.__$bidder = this.$container.find('[data-bidder]');

    /** @type {Array<jQuery>} */
    this.__$stateSvgs = this.$container.find('[data-state-svg]');

    /** @type {Array<jQuery>} */
    this.__$stateMsgs = this.$container.find('[data-state-msg]');

    /** @type {Array<jQuery>} */
    this.__$stops1 = this.$container.find('.stop1');

    /** @type {Array<jQuery>} */
    this.__$stops2 = this.$container.find('.stop2');

    // bind click button
    this.__$bid.on('click', this.onBid.bind(this));
  }

  /**
   * Add new state
   * @param {State} stateObject
   */
  addState (stateObject) {
    let stateId = stateObject.id;
    this.statesMap[stateId] = stateObject;

    // here we could or should or push the new state
    // at the specified index in the array but I don't really
    // see the point of this at this stage. We use state ids
    // now anyway to change the item style through css
    this.states.push(stateObject);

    return stateObject;
  }

  /**
   * Set state updating UI and running the animation
   *
   * It just updates the state animation/color and text msg,
   * e.g. 'bidding, going once, going twice, sold', it also adds an attr
   * on the item container so that we can tweak the style of item
   * differently for each state.
   *
   * @param {?Number|String|State} givenState The state index `0,1,2,3`,
   *                                          or id (e.g. 'verkocht') or
   *                                          new custom object (see the
   *                                          state.defaults.js to see the
   *                                          anatomy of a state object)
   * @param {?Number}               duration  The duration in seconds
   * @param {?Number}               startFrom The resuming point in seconds
   * @return {Item}                           The item instance
   */
  setState (givenState, duration, startFrom) {
    // set current state
    if ($.isPlainObject(givenState)) {
      // if we are resuming just assign the given state
      if ((givenState.second && givenState.second > 0) || startFrom) {
        this.state = givenState;
      } else {
        // either by using the given object
        this.state = this.addState(givenState);
      }
    }
    else if (this.states[givenState]) {
      // or by grabbing the state with its idx value
      this.state = this.states[givenState];
    }
    else if (this.statesMap[givenState]) {
      // or by grabbing the state with its idx value
      this.state = this.statesMap[givenState];
    }
    else {
      // or by grabbing the state with the initial state id value
      this.state = this.statesMap[this._initialStateId];
    }
    if (!this.state) {
      return;
    }
    // set current state second
    this.state.second = startFrom || 0;

    // set current state duration
    if (duration) {
      this.state.duration = duration;
    }
    // set cycle duration
    this._cycleDuration = this.state.duration - this.state.second;

    // display state message
    if (this.__$stateMsgs && this.__$stateMsgs.length) {
      for (let i = 0; i < this.__$stateMsgs.length; i++) {
        this.__$stateMsgs[i].textContent = this.state.text;
      }
    }

    // display state style through css
    this.$container.attr(this.opts.attrState, this.state.id);

    let palette = defaultColors[this.state.id];
    if (palette) {
      if (this.__$stops1 && this.__$stops1.length) {
        for (let j = 0; j < this.__$stops1.length; j++) {
          this.__$stops1[j].setAttribute('stop-color', palette[0]);
        }
      }
      if (this.__$stops2 && this.__$stops2.length) {
        for (let k = 0; k < this.__$stops2.length; k++) {
          this.__$stops2[k].setAttribute('stop-color', palette[1]);
        }
      }
    }

    // ... and fires the animation
    this._runAnimation();
    // start the timer...
    this._startCycle();

    // callback
    this.opts.onState(this);
    // callback on last state ('verkocht')
    if (this.state.id === 'verkocht' || this.state.isLast) {
      this.opts.onEnd(this);
    }

    return this;
  }

  /**
   * Resume state
   * @param  {State} state The state object
   * @return {Item}        The item instance
   */
  resumeState (state) {
    // create a new state object to don't interfere with other items
    // states
    let newState = $.extend({}, state);
    this.setState(newState, null, state.second);
    return this;
  }

  /**
   * Set data
   *
   * @param  {Object} data Data from server that need to update the UI
   */
  setData (data) {
    if (data['current_price']) {
      this.__$price.text(data['current_price']);
    }
    if (data['highest_bidder']) {
      this.__$bidder.text(data['highest_bidder']);
    }
  }

  /**
   * User has clicked bid
   */
  onBid () {
    this.opts.onBid(this.item);
    // console.log('bid! newSeconds:', newSeconds);
  }

  /**
   * Destroy item instance
   */
  destroy () {
    if (this._animations && this._animations.length) {
      for (let i = 0; i < this._animations.length; i++) {
        this._animations[i].destroy();
      }
    }
    this._clearCycle();
  }

  /**
   * Start single cycle timer
   */
  _clearCycle () {
    if (this._cycle) {
      window.clearInterval(this._cycle);
      this._cycle = null;
    }
  }

  /**
   * Start single cycle timer
   */
  _startCycle () {
    this._clearCycle();
    this._cycle = window.setInterval(this._onCycleTick.bind(this), 1000);
  }

  /**
   * On second tick, it runs every second using `setInterval`, it
   * coordinates the animation states.
   */
  _onCycleTick () {
    // maybe trigger expiration behaviour
    this._maybeTriggerExpiration();

    // increment second
    this.state.second++;

    // api callback
    // this.opts.onSecond(this.state.second, this.state, this);

    if (this.state.second >= this.state.duration) {
      this._clearCycle();
    }
  }

  /**
   * Set expiration attribute to trigger further animations through css
   *
   * @param {string} The expiration name to set on the attribute
   */
  _updateUiExpiration (name) {
    this.$container.attr(this.opts.attrExpiration, name);
  }

  /**
   * Loop through the expiration breakpoints of the given state
   * and check if the current second has past the expiration breakpoint
   * (which is defined in percentage because the state duration is dynamic,
   * so we need to calculate the breakpoint in seconds).
   */
  _maybeTriggerExpiration () {
    let expirations = this.state.expirations;
    if (!expirations) {
      return;
    }

    // expirations array must be set in order from lower to higher percentage
    for (let i = expirations.length; i--;) {
      let expiration = expirations[i];
      let secondBreakpoint = (this.state.duration / 100) * expiration.percent;

      if (this.state.second > secondBreakpoint) {
        this._updateUiExpiration(expiration.name);
        return;
      }
    }
    // otherwise reset the expiration attribute
    this._updateUiExpiration('');
  }

  /**
   * It makes one full loop with the given duration or by reading the current
   * state duration
   */
  _runAnimation () {
    if (this._animations && this._animations.length) {
      for (let i = 0; i < this._animations.length; i++) {
        this._animations[i].destroy();
      }
    }

    // init animations
    this._animations = [];
    if (this.__$stateSvgs && this.__$stateSvgs.length) {
      for (let j = 0; j < this.__$stateSvgs.length; j++) {
        // console.log('run for ', this.__$stateSvgs[i])
        this._animations.push(new SvgPieTimer({
          element: this.__$stateSvgs[j],
          duration: this.state.duration, // seconds
          startFrom: this.state.second // seconds
        }));
      }
    }
  }

  /**
   * Get object from array
   *
   * @param  {Array} array
   * @param  {string} value
   * @return {Object}       The lookup object
   */
  _getObjFromArray (array, value) {
    let lookupObject = {};
    for (let i = 0, l = array.length; i < l; i++) {
      lookupObject[array[i][value]] = array[i];
    }
    return lookupObject;
  }
}

// export to public API
api['Item'] = Item;

export default Item;
