import $ from 'jquery';
import api from './api';

class CookieAnnouncement {

  constructor () {
    // bootstrap
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    // console.log('app.js initialized');
  }

  /**
   * On document ready
   */
  $onReady () {
    const self = this;

    const $container = $('#js-cookieannouncement');
    const $dismissTriggers = $('.js-cookieannouncement-dismiss');

    if (!$container.length) {
      return;
    }

    const cookieValue = this._readCookie('cookieannouncement');

    if (cookieValue !== 'shown') {
      setTimeout(function () {
        $container.addClass('in');
      }, 500);

      $dismissTriggers.on('click', () => {
        $container.removeClass('in');
        this._createCookie('cookieannouncement', 'shown', 365);
      });
    };
  }

  /**
   * Create cookie
   * @see https://stackoverflow.com/a/24103596/1938970
   *
   * @param  {String} name
   * @param  {Mixed}  value
   * @param  {Number} days
   */
  _createCookie (name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
  }

  /**
   * Read cookie
   * @see https://stackoverflow.com/a/24103596/1938970
   *
   * @param  {String} name
   * @return {?String}
   */
  _readCookie (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  }

  _eraseCookie (name) {
    this._createCookie(name,"",-1);
  }
}

new CookieAnnouncement();
