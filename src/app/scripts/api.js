// allow this to be set from outside of this script, e.g. from the backend
const api = window['hellospecial'] || {
  'opts': {
    'demo': false
  }
};

export default window['hellospecial'] = api;
