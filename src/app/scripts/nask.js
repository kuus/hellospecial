import $ from 'jquery';
import api from './api';

class Nask {

  constructor () {
    this.$onInit();

    // bootstrap
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    // try to immediately hide the container to don't cause a flash of "unstyled" content
    try {
      const containers = document.getElementsByClassName('nask');
      for (let i = 0; i < containers.length; i++) {
        containers[i].style.opacity = 0;
      }
    } catch(e) {}

    this._browser = this._getBrowser();
    this._mobileOS = this._getMobileOS();
    this._permission = this._getPermission();

    // console.log(`Nask->$onInit: mobile os is ${this._mobileOS} browser detected is ${this._browser}, permission is ${this._permission}`);
  }

  /**
   * On document ready
   */
  $onReady () {
    const $container = $('.nask');

    if (!$container.length) {
      return;
    }

    this.__$container = $container;

    const view = this._getViewName();
    this.setView(view);

    if (view === 'request' || view === 'request-mobile') {
      this.ask();
    }

    this.setEnv(this._getEnvName());

    this.__$container.addClass('nask--ready').css('opacity', 1);

    // @@disabled for now, because it wouldn't work anyway...
    // @see https://stackoverflow.com/a/31868708/1938970
    // $container.find('.nask__askbutton').click(() => {
    //   this.ask()
    // });
  }

  /**
   * Get browser
   *
   * based on @link https://stackoverflow.com/a/9851769/1938970
   *
   * @return {String}
   */
  _getBrowser () {
    // Opera 8.0+
    if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
      return 'opera';
    }

    // Firefox 1.0+
    if (typeof InstallTrigger !== 'undefined') {
      return 'firefox';
    }

    // Safari 3.0+ "[object HTMLElementConstructor]"
    if (/constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification))) {
      return 'safari';
    }

    // Internet Explorer 6-11
    if (/*@cc_on!@*/false || !!document.documentMode) {
      return 'ie';
    }

    // Edge 20+
    if (!!window.StyleMedia) {
      return 'edge';
    }

    // Chrome 1+
    if (!!window.chrome && !!window.chrome.webstore) {
      return 'chrome';
    }
  }

  /**
   * Determine the mobile operating system.
   * This function returns one of 'ios', 'Android', 'Windows Phone', or false.
   *
   * @link https://stackoverflow.com/a/21742107/1938970
   *
   * @returns {String|boolean} 'windows|android|ios|false'
   */
  _getMobileOS () {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      return 'windows';
    }

    if (/android/i.test(userAgent)) {
      return 'android';
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      return 'ios';
    }

    return false;
  }

  /**
   * Get notifications permissions
   *
   * @return {String} 'denied|granted|default|unsupported'
   */
  _getPermission () {
    if (window.Notification && window.Notification.permission) {
      return window.Notification.permission;
    }
    return 'unsupported';
  }

  /**
   * Get view name
   *
   * Views:
   * 'granted': Notification are granted already, we don't ask anything
   * 'request': On Android notifications are denied by the user, we ask to enable them
   * 'request-mobile': On Android notifications are denied by the user, we ask to enable them
   * 'allowing': On desktop notifications are denied by the user, the allow box is opened
   * 'allowing-mobile': On Android notifications are denied by the user, the allow box is opened
   * 'thanks': On desktop notification permission has just been granted by the user
   * 'thanks-mobile': On Android notification permission has just been granted by the user
   * 'other': Desktop browser other than Chrome. Firefox, Opera, no notifications system
   * 'other-mobile': Mobile browser other than Android, no notifications system
   *
   * @param  {?String} viewFamily
   * @return {String}
   */
   _getViewName (viewFamily) {
    if (viewFamily === 'allowing') {
      return this._mobileOS ? 'allowing-mobile' : 'allowing';
    }
    else if (viewFamily === 'thanks') {
      return this._mobileOS ? 'thanks-mobile' : 'thanks';
    }

    // if (['chrome', 'firefox', 'opera'].indexOf(this._browser) === -1) {
    // }

    if (this._permission === 'unsupported') {
      return this._mobileOS ? 'other-mobile' : 'other';
    }
    else if (this._permission === 'default') {
      return this._mobileOS ? 'request-mobile' : 'request';
    }
    else if (this._permission === 'denied') {
      return this._mobileOS ? 'request-mobile' : 'request';
    }
    else if (this._permission === 'granted') {
      return 'granted';
    }

    return 'none';
  }

  /**
   * Get env name
   *
   * @return {String}
   */
  _getEnvName () {
    return this._mobileOS === 'android' ? 'android' : this._browser;
  }

  /**
   * Set view on the UI
   *
   * @public
   * @param {String} view
   */
  setView (view) {
    this.__$container.attr('data-nask-view-is', view);
    console.log(`Nask: set view name to "${view}"`);
    return this;
  }

  /**
   * Set environment on the UI
   *
   * @public
   * @param {String} env
   */
  setEnv (env) {
    this.__$container.attr('data-nask-env-is', env);
    console.log(`Nask: set env to "${env}"`);
    return this;
  }

  /**
   * Ask for permission
   *
   * @param {?Function} callback
   * @public
   *
   * @return {this}
   */
  ask (callback) {
    if (this._permission === 'unsupported') {
      console.log(`Nask: Notification are not supported`);
      return;
    }
    console.log(`Nask: asking for permission`);

    this.setView(this._getViewName('allowing'));

    Notification.requestPermission((permission) => {
      if (callback) {
        callback(permission);
      }

      if (permission === 'granted') {
        // const notification = new Notification('Thanks');
        this.setView(this._getViewName('thanks'));
      }
      else if (permission === 'denied') {
        this.setView(this._getViewName('request'));
      }
    });

    return this;
  }
}

// export to public API
api['nask'] = new Nask();

export default Nask;
