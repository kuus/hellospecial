import $ from 'jquery';
// import tether from 'tether';
import 'bootstrap/js/src/collapse';
import 'bootstrap/js/src/alert';
import 'bootstrap/js/src/modal';
import 'bootstrap/js/src/tooltip';
import 'bootstrap/js/src/dropdown';
// import './bootstrap-4-dev/collapse'; // 'bootstrap/js/src/collapse';
// import './bootstrap-4-dev/alert'; // 'bootstrap/js/src/alert';
// import './bootstrap-4-dev/modal'; // 'bootstrap/js/src/modal';
// import './bootstrap-4-dev/tooltip'; // 'bootstrap/js/src/tooltip';
// import './bootstrap-4-dev/dropdown'; // 'bootstrap/js/src/dropdown';

// import 'jquery-match-height/dist/jquery.matchHeight';
// import 'datatables.net';
// import 'slick-carousel/slick/slick.js';

import api from './api';
import './demo';
import './abtesting';
import './cookieannouncement'
import './nask';
import './faq';
import './item';
import './itemDetail';
import './browsers';
import './checkout';
import './seller';
import './account';
import './countdown';
import './ButtonLoader';

class App {

  constructor () {
    // bootstrap
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    // console.log('app.js initialized');
    this._updateCopyrightYear();
    this._accountMenu();
  }

  /**
   * On document ready
   */
  $onReady () {
    // console.log('app.js document is ready');
    this.__$itemCols = $('.hsItem__col');

    this._$win = $(window);

    // this._mobileImages();
    this._matchColsHeight();
    this._manageCatsSlider();
    this._detectOS();
    this._initModals();
    this._initForms();
    this._initTables();
    this._initDropdowns();
    this._initAuthModal();
    this._$win.resize(this._matchColsHeight.bind(this));
    this._$win.resize(this._manageCatsSlider.bind(this));
    // this._$win.resize(this._mobileImages.bind(this));
  }

  /**
   * Init tooltips
   *
   */
  _initTooltips () {
    // bootstrap tooltips
    // $('.js-tooltip').tooltip({});
    $('.shippingtime-tooltip').tooltip({
      animation: false,
      offset: '-10px 0',
      // delay: { hide: 1000000 }, // for debugging
      template: '<div class="tooltip tooltip-top tooltip--light shippingtime__tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    });
    // $('.shippingtime').each(function () {
    //   $(this).tooltip({
    //     template: '<div class="tooltip tooltip--light" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
    //     container: this,
    //     offset: '0 -80%',
    //     delay: { hide: 1000000 } // for debugging
    //   });
    // });
  }

  /**
   * Manage item card's mobile images
   */
  _mobileImages () {
    if ($(window).width() < 992) {
      let mobileProductImages = document.getElementsByClassName('hsItemCard__bottom-img');
      if (mobileProductImages) {
        for (let i = 0; i < mobileProductImages.length; i++) {
          let tmpImg = new Image();
          let img = mobileProductImages[i];
          tmpImg.src = img.src;

          let $wrapper = $(img.parentNode);

          let imgCssString = '';

          tmpImg.onload = function () {
            let wrapperWidth = $wrapper.outerWidth(true);//.offsetWidth;
            let wrapperHeight = $wrapper.outerHeight(true);// .offsetHeight;
            let imgWidth = tmpImg.naturalWidth;
            let imgHeight = tmpImg.naturalHeight;
            let a = Math.max(wrapperHeight, img.height);
            console.log('wrapperHeight', wrapperHeight, 'wrapperHeight', wrapperHeight);
            if (imgHeight > imgWidth) {
              imgCssString += `max-width:${wrapperWidth}px;height:auto;left:0;top:50%;margin-top:-${img.height / 2}px;`;
            } else {
              imgCssString += `max-height:${wrapperHeight}px;width:auto;left:50%;top:0;margin-left:-${img.width / 2}px;`;
            }

            img.style.cssText = imgCssString + 'opacity:1;';
          }
        }
      }
    }
  }

  /**
   * Detect OS
   */
  _detectOS () {
    let $html = $('html');
    let userAgent = window.navigator ? window.navigator.userAgent || '' : '';

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      $html.addClass('is-ios');
    }
  }

  /**
   * Match the height of the item cards' columns
   */
  _matchColsHeight () {
    if (this.__$itemCols.length) {
      if ($(window).width() >= 992) {
        this.__$itemCols.matchHeight({});
      } else {
        this.__$itemCols.matchHeight({ remove: true });
      }
    }
  }

  /**
   * Manage categories slider
   */
  _manageCatsSlider () {
    const $subheaderList = $('.appSubheader__list');
    const $subheaderLinks = $('.appSubheader__link');
    let listFullWidth = 0;
    let currentLinkIdx;
    let oneSlideWidth;

    /**
     * Show hint scrolls only when it's needed
     *
     */
    const _maybeShowHintScrolls = function (e, slick, current, nextIdx) {
      if (nextIdx < $subheaderLinks.length - 3) {
        $subheaderList.addClass('slick-hint-next');
      } else {
        $subheaderList.removeClass('slick-hint-next');
      }

      if (nextIdx > 1) {
        $subheaderList.addClass('slick-hint-prev');
      } else {
        $subheaderList.removeClass('slick-hint-prev');
      }
    }

    if (!$subheaderList.length) {
      return;
    }
    $subheaderLinks.each((idx, item) => {
      let $item = $(item);
      oneSlideWidth = $item.width();
      listFullWidth = listFullWidth + oneSlideWidth;
      if ($item.hasClass('active')) {
        currentLinkIdx = idx;
      }
    });

    // if we have a slider
    if ($subheaderList.width() < listFullWidth) {
      // just update the current slide
      if (this._catsSlider) {
        if (currentLinkIdx || currentLinkIdx === 0) {
          this._catsSlider.trigger('slickGoTo', currentLinkIdx);
        }
      // or init the slider
      } else {
        this._catsSliderHtmlPreSlick = $subheaderList.html();

        this._catsSlider = $subheaderList.slick({
          // slidesToShow: 4, // $subheaderLinks.length,
          dots: false,
          arrows: false,
          variableWidth: true,
          slidesToScroll: 1,
          centerMode: this._determineIfCenterMode(listFullWidth, oneSlideWidth, currentLinkIdx),
          infinite: false,
          initialSlide: currentLinkIdx
        });

        _maybeShowHintScrolls(null, null, null, currentLinkIdx);

        this._catsSlider.on('beforeChange', _maybeShowHintScrolls);
      }
    // if we don't want to have a slider
    } else {
      if (this._catsSlider) {
        this._catsSlider.slick('unslick')
          .removeClass('slick-hint-prev')
          .removeClass('slick-hint-next');
        $subheaderList.html(this._catsSliderHtmlPreSlick);
        this._catsSlider = null;
      }
    }
  }

  /**
   * Determine if the categories slider should put the current icon
   * in the center, this is to prevent to have a partially empty slider
   * on the left hand side.
   *
   * @param  {Number}  listFullWidth
   * @param  {Number}  oneSlideWidth
   * @param  {Number}  currentLinkIdx
   * @return {boolean}
   */
  _determineIfCenterMode (listFullWidth, oneSlideWidth, currentLinkIdx) {
    const containerWidth = $(window).width();
    const slidesThatFit = containerWidth / oneSlideWidth;
    // console.log(`currentLinkIdx = ${currentLinkIdx}, slidesThatFit = ${slidesThatFit}`);
    // if (currentLinkIdx < slidesThatFit) {
    //   return false;
    // }
    if (currentLinkIdx === 0) {
      return false;
    }
    return true;
  }

  /**
   * For centered modal
   *
   * @see  https://stackoverflow.com/a/41377502/1938970
   */
  _initModals () {
    $('.modal-dialog').on('click tap', function(e) {
      const __$modalDialog = $(this);
      if ($(e.target).hasClass('modal-dialog')) {
        __$modalDialog.parent().modal('hide');
      }
    });
  }

  /**
   * Init forms (validation)
   */
  _initForms () {
    $('.form-validate').validate({
      errorClass: 'has-danger',
      validClass: 'has-success',
    });
  }

  /**
   * Init tables
   */
  _initTables () {
    // datable on 'my account' pages
    $('.datatable').DataTable({
      searching: false,
      ordering: false,
      drawCallback: this._initTooltips.bind(this)
    });
  }

  /**
   * Init dropdowns
   */
  _initDropdowns () {
    // dropdown
    $('#hsLang__header')
      .on('show.bs.dropdown', () => {
        $('#hsHeader').addClass('last-dropdown-open');
      })
      .on('hide.bs.dropdown', () => {
        $('#hsHeader').removeClass('last-dropdown-open');
      });

    // $('.navselector') // 991 change to <option>
    $('.JSnavsel__select').on('change', function () {
      window.location.href = this.value;
      // getAttribute('data-href');
    });
  }

  /**
   * Update copyright year
   */
  _updateCopyrightYear() {
    var el = document.getElementById('js-copyyear');
    if (el) {
      el.innerHTML = new Date().getFullYear();
    }
  }

  /**
   * Account menu
   */
  _accountMenu () {
    var el = document.getElementById('js-myaccount_btn');
    if (el) {
      el.onclick = function () {
        document.body.classList.add('hsAccount--menu-open');
      };
    }
  }

  /**
   * Init authentication modals
   */
  _initAuthModal () {
    let $authModal = $('#hsAuthModal');
    this.__$authModal = $authModal;
    if (!$authModal.length) {
      return;
    }

    $authModal.on('show.bs.modal', function (e) {
      $('body').addClass('appAuthModal--open');
    });
    $authModal.on('hidden.bs.modal', function (e) {
      $('body').removeClass('appAuthModal--open');
    });
    $authModal.modal('hide');
    // $authModal.modal('hide');

    $(document).on('click', '[data-authmodal]', function (e) {
      e.preventDefault();
      let btn = this;
      let $btn = $(this);
      let view = this.getAttribute('data-authmodal');
      if (!view) {
        throw new Error('`data-authmodal` must have a value (e.g. `login` or `register`)');
        return;
      }
      $authModal.attr('data-view', view);
      try {
        $authModal.modal('show');
      } catch (e) {
        console.log(e);
      }
    });
  }

  /**
   * Prompt login modal
   *
   * @public
   */
  promptLogin () {
    if (this.__$authModal) {
      this.__$authModal.attr('data-view', 'login');
      this.__$authModal.modal();
    }
  }

  /**
   * Prompt register modal
   *
   * @public
   */
  promptRegister () {
    if (this.__$authModal) {
      this.__$authModal.attr('data-view', 'register');
      this.__$authModal.modal();
    }
  }

  /**
   * Update UI (Erwin call's this when opening the item modal)
   *
   * @public
   */
  updateUI () {
    try {
      api['itemDetail'].$onReady();
    } catch (e) {}
  }
}

// export to public API
api['app'] = new App();
