import $ from 'jquery';
import api from './api';
import Item from './item';

class Demo {

  constructor () {
    const host = window.location.host;
    if (host === 'kuus.github.io' || host.split(':')[0] === 'localhost') {
      this.init();
    }
  }

  /**
   * Init
   */
  init () {
    /** @type {Number} @@demoonly Max seconds for total loop of four state */
    this._loopMaxSeconds = 12;

    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * On document ready
   */
  $onReady () {
    // this.initModals();
    this.initItems();
    this.checkout();
  }

  /**
   * Initialize items
   */
  initItems () {
    $('.hsItem').each((index, element) => {
      let initialState = undefined;
      if ($(element).hasClass('appItemDetail')) {
        initialState = 'geboden';
      }
      let item = new Item({
        id: index,
        elem: element,
        // state: 3,
        onState: this._setNextState.bind(this),
        // onEnd: this._onEnd.bind(this),
      });
      item.setState(initialState, this._getAverageStateDuration(item));
    });
  }

  /**
   * Get average state duration
   *
   * Durations are calculated dynamically dividing equally the max time by the
   * number of states
   *
   * @param  {Item}   item
   * @return {Number} Duration in seconds
   */
  _getAverageStateDuration (item) {
    return this._loopMaxSeconds / item.states.length;
  }

  /**
   * Set next state
   *
   * @param  {Item}   item
   */
  _setNextState (item) {
    setTimeout(() => {
      // console.log('item.id', item.state.id, 'idx', item.state.idx)
      if (item.state.idx === item.states.length - 1) {
        item.setState(0, this._getAverageStateDuration(item));
      } else {
        item.setState(item.state.idx + 1, this._getAverageStateDuration(item));
      }
    }, this._getAverageStateDuration(item) * 1000);
  }

  /**
   * Initialize various modals, e.g. winning bet modal
   */
  initModals () {
    // $('#hsWinModal').modal('show');
    $('#appPayreminderModal').modal('show');
  }

  /**
   * Checkout fake behaviour
   */
  checkout () {
    // breaadcrumbs
    $('.hsCheckout__breadcrumb').on('click', (e) => {
      let checkout = window.hellospecial.checkout;
      const el = e.target.parentNode;
      if (el) {
        const step = $(el).index();
        if (step === 1) {
          if (checkout.isFormValid()) {
            checkout.goToStep(step);
          }
        } else {
          checkout.goToStep(step);
        }
        e.stopPropagation();
        return false;
      }
    });

    // to step2
    $('.hsCheckout__tostep2').on('click', function (e) {
      e.preventDefault();
      let checkout = window.hellospecial.checkout;
      if (checkout.isFormValid()) {
        checkout.goToStep(1);
      }
    });
  }
}

let demo = new Demo();

// export to public API
api['demo'] = demo;

export default demo;
