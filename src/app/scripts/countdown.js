import $ from 'jquery';
import api from './api';

class Countdown {

  constructor () {
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
  }

  /**
   * On document ready
   */
  $onReady () {
    this.__$wrapper = $('.countdown');

    if (!this.__$wrapper.length) {
      return;
    }

    this.__$days = this.__$wrapper.find('[data-countdown="days"]');
    this.__$hours = this.__$wrapper.find('[data-countdown="hours"]');
    this.__$minutes = this.__$wrapper.find('[data-countdown="minutes"]');
    this.__$seconds = this.__$wrapper.find('[data-countdown="seconds"]');

    this._startCounter();
  }

  /**
   * Manually init
   */
  init () {
    this.$onReady();
  }

  /**
   * Start counter;
   * @return {[type]} [description]
   */
  _startCounter () {
    this._seconds = 0;
    if (this._interval) {
      clearInterval(this._interval);
    }
    this._interval = setInterval(() => {
      this._seconds = this._seconds + 1;
      this._update();
    }, 1000);
  }

  /**
   * Update UI every second
   */
  _update () {
    const currentSeconds = parseInt(this.__$seconds.html(), 10);
    const currentMinutes = parseInt(this.__$minutes.html(), 10);
    const currentHours = parseInt(this.__$hours.html(), 10);
    const currentDays = parseInt(this.__$days.html(), 10);

    let newSeconds = Math.max(-1, Math.min(currentSeconds - 1, 60));
    let newMinutes = currentMinutes;
    let newHours = currentHours;
    let newDays = currentDays;

    if (newSeconds === -1) {
      newSeconds = 59;
      newMinutes = Math.max(-1, Math.min(currentMinutes - 1, 59));

      if (newMinutes === -1) {
        newMinutes = 59;
        newHours = Math.max(-1, Math.min(currentHours - 1, 23));

        if (newHours === -1) {
          newHours = 23;
          newDays = Math.max(-1, Math.min(newDays - 1, 364));
        }
      }
    }

    this.__$seconds.html(this._pad(newSeconds));
    this.__$minutes.html(this._pad(newMinutes));
    this.__$hours.html(this._pad(newHours));
    this.__$days.html(this._pad(newDays));
  }

  /**
   * Pad numbers (add leading zero)
   *
   * @param  {Number} num
   * @param  {Number} size
   * @return {String}
   */
  _pad (num, size=2) {
    let s = num + '';
    while (s.length < size) s = '0' + s;
    return s;
  }
}

// export to public API
api['countdown'] = new Countdown();

export default Countdown;
