import api from './api';
import { getI18n } from './i18n';

/**
 * Anatomy of a state object
 *
 * (values preceded by a question mark are optional)
```
{
  id: {string},            // All lowercase, no whitespaces. This is used in the css as well. e.g. ''eenmaal'',
  idx: {Number},           // The index of the state in the states succession. e.g. `1`
  text: {string},          // The displayed text for the state. E.g. `'Eenmaal...'`,
  duration: {?Number},     // Dynamically set by the Item class, it can have a default value though
  isLast: {?boolean},      // Set it to true if you want to indicate that this state is the last. Otherwise
                           // the last is always considered `'verkocht'`
  expirations: {Array<
      Object<
        percent: {Number}, // Number that indicates at which point in time
                           // (expressed in percentage) this expiration breakpoint is triggered
        name: {string}     // All lowercase, no whitespaces. This is used in the css as well. e.g. `'blink'`,
        >
    >}
  [{
    percent: 70,
    name: 'blink'
  }]
}
```
*/

/**
 * The `text` is displayed in the status bar.
 *
 * The `duration` is set dynamically, it's here just for reference.
 *
 * The `expirations` values available are for now:
 * 'blink, close, hurry, final'; to each of them correspond
 * some variations and animation applied through css. Each expiration must
 * declare a `percent` property that indicates the percentage at which point
 * in time trigger the expiration animation (or whatever),
 * e.g. a `percent` value of `50` would trigger at the 14th second for a
 * state that lasts 28s
 */
const defaultStates = [
  {
    id: 'eenmaal',
    idx: 0,
    text: getI18n('eenmaal'),
    duration: null,
    expirations: [{
      percent: 70,
      name: 'blink'
    }]
  }, {
    id: 'andermaal',
    idx: 1,
    text: getI18n('andermaal'),
    duration: null,
    expirations: [{
      percent: 50,
      name: 'hurry'
    }, {
      percent: 70,
      name: 'blink'
    }, {
      percent: 90,
      name: 'final'
    }]
  }, {
    id: 'geboden',
    idx: 2,
    text: getI18n('geboden'),
    duration: null,
    expirations: [{
      percent: 50,
      name: 'hurry'
    }, {
      percent: 70,
      name: 'blink'
    }, {
      percent: 90,
      name: 'final'
    }]
  }, {
    id: 'geboden-timeless',
    idx: 3,
    text: getI18n('geboden-timeless'),
    duration: null
  }, {
    id: 'verkocht',
    idx: 4,
    text: getI18n('verkocht'),
    duration: null
  }, {
    id: 'bieden',
    idx: 5,
    text: getI18n('bieden'),
    duration: null,
    expirations: [{
      percent: 70,
      name: 'blink'
    }]
  }, {
    id: 'winnen',
    idx: 6,
    text: getI18n('winnen'),
    duration: null,
    expirations: [{
      percent: 50,
      name: 'hurry'
    }, {
      percent: 70,
      name: 'blink'
    }]
  }, {
    id: 'geladen',
    idx: 7,
    text: getI18n('geladen'),
    duration: null,
    expirations: [{
      percent: 70,
      name: 'blink'
    }]
  }, {
    id: 'gesloten',
    idx: 8,
    text: getI18n('gesloten'),
    duration: null,
    expirations: [{
      percent: 70,
      name: 'blink'
    }]
  }, {
    id: 'controleren',
    idx: 9,
    text: getI18n('controleren'),
    duration: null
  }, {
    id: 'uitverkocht',
    idx: 10,
    text: getI18n('uitverkocht'),
    duration: null
  },
];

// export to public API
api['defaultStates'] = defaultStates;

export default defaultStates;
