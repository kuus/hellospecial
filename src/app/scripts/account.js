import $ from 'jquery';
import api from './api';

class Account {

  constructor () {
    // bootstrap
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    this._breakpointMobile = 600;
  }

  /**
   * On document ready
   */
  $onReady () {
    // console.log('app.js document is ready');
    this.__$wrapper = $('.hsAccount');

    // bail if we are not on the account page
    if (!this.__$wrapper.length) {
      return;
    }

    this._ttModal();
    this._gegevens();
  }

  /**
   * Track and trace modal
   */
  _ttModal () {
    let $ttModal = $('#hsTtModal');
    if (!$ttModal.length) {
      return;
    }
    $(document).on('click', '.hsAccount__table-tt', function (e) {
      e.preventDefault();
      $ttModal.modal();
      $ttModal.find('.tt__verzendpartij').text(this.getAttribute('data-verzendpartij'));
      $ttModal.find('.tt__code').text(this.getAttribute('data-code'));
      // $ttModal.on('show.bs.modal', function (e) {
      //   // do something...
      // })
    });
  }

  /**
   * Gegevens views switch
   */
  _gegevens () {
    const $btnToEdit = $('#gegevens_switchedit');
    const $viewShow = $('#gegevens_show');
    const $viewEdit = $('#gegevens_edit');

    if (!$btnToEdit.length) {
      return;
    }

    const _switchView = (event, name) => {
      let newView = name;
      let currentView = this._currentView;
      if (event) {
        event.preventDefault();
      }
      if (!newView) {
        newView = location.hash.replace('#', '');
      }
      if (!newView) {
        return;
      }
      if (newView === 'show' && currentView !== 'show') {
        $viewEdit.slideUp();
        $viewShow.slideDown();
        location.hash = this._currentView = 'show';
      } else if (newView === 'edit' && currentView !== 'edit') {
        $viewShow.slideUp();
        $viewEdit.slideDown();
        location.hash = this._currentView = 'edit';
      }
    }

    window.addEventListener('hashchange', _switchView, false);

    _switchView(null, 'show');

    $btnToEdit.click((e) => { _switchView(e, 'edit' )});
  }
}

// export to public API
api['account'] = new Account();

export default Account;
