import $ from 'jquery';
import api from './api';

class ButtonLoader {

  constructor () {
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    $.fn.buttonLoader = function (action) {
      const $this = $(this);

      if (!$this.find('.ButtonLoader__loader').length) {
        $this.append($('<span class="ButtonLoader__loader" />'));
      }
      if (action === 'start') {
        $this.addClass('is-loading');
      }
      else if (action === 'stop') {
        $this.removeClass('is-loading');
      }
    }
  }

  /**
   * On document ready
   */
  $onReady () {
    $(document).on('click', '.ButtonLoader', function () {
      $(this).buttonLoader('start');
    });
  }

}

// export to public API
api['ButtonLoader'] = ButtonLoader;

new ButtonLoader();

export default ButtonLoader;
