import $ from 'jquery';
import api from './api';

class Checkout {

  constructor () {
    // bootstrap
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
    this._breakpointMobile = 600;
  }

  /**
   * On document ready
   */
  $onReady () {
    // console.log('app.js document is ready');
    this.__$wrapper = $('#hsCheckout');

    // bail if we are not on the checkout page
    if (!this.__$wrapper.length) {
      return;
    }

    this.__$htmlBody = $('html, body');
    this.__$form = $('#hsCheckout__form');
    this.__$wrapperLeft = $('.hsCheckout__main-left');
    this.__$breadcrumbs = $('.hsCheckout__breadcrumb');

    this.__$couponArea = $('.hsCheckout__coupon-area');
    this.__$couponTrigger = $('.hsCheckout__coupon-trigger');
    this.__$couponInput = $('.hsCheckout__coupon-input');
    this.__$couponSubmit = $('.hsCheckout__coupon-submit');
    this.__$couponFeedback = $('.hsCheckout__coupon-feedback');

    this.__$addressPrefilled = $('#hsAddress__prefilled');
    this.__$inputVoornam = $('#voornaam');
    this.__$inputAchternaam = $('#achternaam');
    this.__$inputStraat = $('#straat');
    this.__$inputHuisnummer = $('#huisnummer');
    this.__$inputPostcode = $('#postcode');
    this.__$inputWoonplaats = $('#woonplaats');
    this.__$inputLand = $('#land');
    this.__$addressDisplay = $('#hsAddress__display');

    this.__$costWinbedrag = $('.hsCheckout__cost-winbedrag');
    this.__$costWinbedragDel = $('.hsCheckout__cost-winbedrag--del');
    this.__$costVerzendkosten = $('.hsCheckout__cost-verzendkosten');
    this.__$costVeilingkosten = $('.hsCheckout__cost-veilingkosten');
    this.__$costTotaal = $('.hsCheckout__cost-totaal');

    this.__$expander = $('.hsCheckout__expander');
    this.__$step0 = $('#hsCheckout__step-0');
    this.__$step1 = $('#hsCheckout__step-1');
    this.__$btnToStep1 = $('.hsCheckout__tostep1');
    this.__$btnToStep2 = $('.hsCheckout__tostep2');
    this.__$summary = $('.hsCheckout__main-summary');

    this.__$paymentMethods = $('input[name="pay-method"]');
    this.__$paymentBanks = $('input[name="pay-bank"]');
    this.__$methodIdeal = $('.hsCheckout__method--ideal');
    this.__$bankSelector = $('.hsCheckout__banks');
    this.__$btnSubmit = $('.hsCheckout__submit');

    this.__$details = $('.hsCheckout__details');

    // animate steps @@disabled
    // const minHeight = this.__$step0.outerHeight() * 1.5;
    // this.__$wrapper.css('min-height', minHeight);
    // this.__$wrapperLeft.css('min-height', minHeight);

    this.manageValidation();

    this._bindUI();

    // start by step 0 on load
    this.goToStep(0);
  }

  /**
   * Manage form validation
   *
   * @see options here https://jqueryvalidation.org/validate/
   * @@todo perhaps use this to style the validator on the all
   * website: `$.validator.setDefaults({})`
   */
  manageValidation () {
    this._validation = this.__$form.validate({
      // submitHandler: () => {
      //   this.goToStep(1);
      // },
      errorClass: 'has-danger',
      validClass: 'has-success',
      // errorElement: '<div class="form-control-feedback">',
      // highlight: function(element, errorClass, validClass) {
      //   $(element).parent().addClass(errorClass).removeClass(validClass);
      // },
      // unhighlight: function(element, errorClass, validClass) {
      //   $(element).parent().removeClass(errorClass).addClass(validClass);
      // }
    });
    console.log(this._validation)
  }

  /**
   * Bind UI
   */
  _bindUI () {
    const self = this;

    // coupon area
    self.__$couponArea.hide();
    self.__$couponFeedback.hide();
    self.__$couponSubmit.attr('disabled', true);
    self.__$couponTrigger.on('click', function () {
      self.__$couponTrigger.slideUp(100, function () {
        self.__$couponArea.slideDown();
      });
    });
    self.__$couponInput.on('input', function () {
      if (this.value) {
        self.__$couponSubmit.attr('disabled', false);
      } else {
        self.__$couponSubmit.attr('disabled', true);
      }
    });
    self.__$couponSubmit.on('click', function () {
      self.__$couponFeedback.slideDown();
      self.applyCoupon(self.__$couponFeedback.data('discount'));
      self.__$couponSubmit.attr('disabled', true);
      self.__$couponInput.off('input');
    });

    // mobile expander
    if (window.innerWidth < self._breakpointMobile) {
      self.__$summary.hide();
      self.__$expander.removeClass('expanded').show();
    }
    self.__$expander.on('click', function () {
      self.__$summary.slideToggle();
      self.__$expander.toggleClass('expanded');
    });

    // address choice
    self.__$details.hide();
    if (!!$('input[name="addresschoice"]:checked').val()) {
      self.__$details.slideDown();
    }
    $('input[name="addresschoice"]').on('change', function () {
      self.__$details.slideToggle(!!this.value);
    });

    // to step1
    self.__$btnToStep1.on('click', function (e) {
      e.preventDefault();
      self.goToStep(0);
    });

    // payment method
    self.__$bankSelector.hide();
    self.__$btnSubmit.hide();
    self.__$paymentMethods.on('change', function () {
      if (this.value === 'ideal') {
        self._maybeShowSubmitBtn(self._isBankSelected);
        self.__$bankSelector.slideDown();
      } else {
        self.__$btnSubmit.slideDown();
        self.__$bankSelector.slideUp();
      }
    });

    // bank selection
    self.__$paymentBanks.on('change', function () {
      self._isBankSelected = !!this.value; // make it a boolean
      self._maybeShowSubmitBtn(!!this.value);
    });
  }

  /**
   * Maybe show the submit button
   * @param  {boolean} showIt
   */
  _maybeShowSubmitBtn (showIt) {
    if (showIt) {
      this.__$btnSubmit.slideDown();
    } else {
      this.__$btnSubmit.slideUp();
    }
  }

  /**
   * Go to step
   *
   * @param  {Number} step e.g. 0, 1, 2, etc.
   */
  goToStep (step) {
    // first remove class from all breadcrumbs
    this.__$breadcrumbs.removeClass('active');

    // add the active class
    this.__$breadcrumbs.filter((index, elem) => {
      if (index === step) {
        // highlight current breadcrumb
        $(elem).addClass('active');

        // useful for global styling and hiding/showing based on current step
        this.__$wrapper.attr('data-step', step);
      }
    });

    this.__$htmlBody.animate({ scrollTop: 0 });

    if (step === 1) {
      this.manageAddress();
    }
  }

  /**
   * Is form valid
   *
   * @return {Boolean}
   */
  isFormValid () {
    return this.__$form.valid();
  }

  /**
   * Apply coupon
   * @param  {Number} discount The amount of discount in cash
   */
  applyCoupon (discount) {
    const currentPrice = this.__$costWinbedrag.data('val');
    const newPrice = parseFloat(currentPrice) - parseFloat(discount);

    // fill deleted price
    this.__$costWinbedragDel.html(this.formatPrice(currentPrice));

    // update winbedrag price (also it's data raw number value)
    this.__$costWinbedrag.html(this.formatPrice(newPrice)).data('val', newPrice);

    // update totaal price
    this.__$costTotaal.html(this.formatPrice(this.getTotalPrice()));
  }

  /**
   * Get total price by summing values on the DOM
   * @return {Number}
   */
  getTotalPrice () {
    const a = this.getPriceFrom(this.__$costWinbedrag);
    const b = this.getPriceFrom(this.__$costVerzendkosten);
    const c = this.getPriceFrom(this.__$costVeilingkosten);
    return a + b + c;
  }

  /**
   * Get price from jQuery DOM element
   * @param  {jQuery} $elem
   * @return {Number}
   */
  getPriceFrom ($elem) {
    let asString;
    let val = $elem.data('val').toString();

    if (val) {
      asString = val.replace(',', '.');
    } else {
      asString = '0';
    }
    return parseFloat(asString);
  }

  /**
   * Add zeroes
   * @see http://stackoverflow.com/a/24039448/1938970
   * @param {Number} value
   * @return {String}
   */
  formatPrice (value) {
    const asNumber = parseFloat(value);
    // replace dot with comma as in design
    const price = asNumber.toFixed(2).replace('.', ',');
    return this.getCurrency() + ' ' + price;
  }

  /**
   * Get currency
   * @return {String}
   */
  getCurrency () {
    return '€';
  }

  /**
   * Manage address
   */
  manageAddress () {
    let prefilledAddress;

    if (!$('input[name=addresschoice]:checked').val() && this.__$addressPrefilled.html()) {
      prefilledAddress = this.__$addressPrefilled.html();
    }
    else {
      const voornam = this.__$inputVoornam.val();
      const achternaam = this.__$inputAchternaam.val();
      const straat = this.__$inputStraat.val();
      const huisnummer = this.__$inputHuisnummer.val();
      const postcode = this.__$inputPostcode.val();
      const woonplaats = this.__$inputWoonplaats.val();
      const land = this.__$inputLand.find(':selected').text();

      prefilledAddress = `${voornam} ${achternaam}, ${straat} ${huisnummer},
        <br>${postcode}, ${woonplaats}, ${land}`;
    }

    this.__$addressDisplay.html(prefilledAddress);
  }
}

// export to public API
api['checkout'] = new Checkout();

export default Checkout;
