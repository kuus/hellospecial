import api from './api';

const defaultColors = {
  'eenmaal': ['#6DBF52', '#BCD53A'],
  'andermaal': ['#EC282E', '#F58639'],
  'geboden': ['#8756A3', '#BC91C0'],
  'geboden-timeless': ['#6b4482', '#a871ad'],
  'verkocht': ['#B2B2B2', '#E6E7E6'],
  'bieden': ['#0B72BA', '#4A9DD6'],
  'winnen': ['#ad005d', 'deeppink'],
  'geladen': ['#a38d8d', '#e2bab7'],
  'gesloten': ['#9d8f8f', '#d7bfbd'],
  'controleren': ['#979090', '#cec4c1']
};

// export to public API
api['defaultColors'] = defaultColors;

export default defaultColors;
