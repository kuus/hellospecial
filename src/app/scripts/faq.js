class Faq {

  constructor () {

    // bootstrap
    this.$onInit();
    $(document).ready(this.$onReady.bind(this));
  }

  /**
   * Init
   */
  $onInit () {
  }

  /**
   * On document ready
   */
  $onReady () {
    if (!$('#hsFaq').length) {
      return;
    }

    const $expandables = $('.expandable__body');
    const $noResults = $('.hsFaq__noresults');
    const highlightOpts = { className: 'hsFaq__mark' };

    $expandables.collapse({
      toggle: false
    });

    $('#hsFaq_filterList').liveFilter('#hsFaq_filterInput', '.expandable', {
      after: function (contains, containsNot) {
        const query = this.value;

        if (!contains.length) {
          $expandables.parent().parent().prev().hide();
          $noResults.show();
        } else {
          $noResults.hide();
          $expandables.parent().parent().prev().show();
        }

        if (!query) {
          // contains.collapse('hide').unhighlight(highlightOpts);
          // containsNot.collapse('hide').unhighlight(highlightOpts);
          $expandables.collapse('hide').unhighlight(highlightOpts);
        } else {
          try {
            contains.find('.expandable__body').collapse('show')
              .unhighlight(highlightOpts)
              .highlight(query, highlightOpts);

            containsNot.find('.expandable__body').collapse('hide')
              .unhighlight(highlightOpts);
          } catch (e) {}
        }
      }
    });

    $('.hsFaq__search').on('submit', this._onSearchSubmit);
    $('#hsFaq_filterSubmit').on('click', this._onSearchSubmit);
  }

  /**
   * On search submit
   *
   * @param  {Object} e Event
   */
  _onSearchSubmit (e) {
    e.preventDefault();
    if ($(window).width() < 600) {
      // $('html, body').animate({
      //   scrollTop: $('.hsFaq__mark').first().offset().top - 70
      // }, 300);
      $('html, body').scrollTop($('.hsFaq__mark').first().offset().top - 70);
    }
  }
}

new Faq();
