import $ from 'jquery';

const a = navigator.userAgent;
const p = navigator.platform;

const browsers = {
  'safari': Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0,
  // 'chrome': !!window.chrome && !opera,
  'ie': /*@cc_on!@*/false || !!document.documentMode,
  // 'mac': p.toUpperCase().indexOf('MAC') >= 0,
  // 'macLike': p.match(/(Mac|iPhone|iPod|iPad)/i) ? true : false,
  'ios': p.match(/(iPhone|iPod|iPad)/i) ? true : false,
  // 'windows': p.toUpperCase().indexOf('WIN')!==-1,
  // 'linux': p.toUpperCase().indexOf('LINUX')!==-1,
  // 'android': /Android/i.test(a),
  // 'blackBerry': /BlackBerry/i.test(a),
  // 'ios': /iPhone|iPad|iPod/i.test(a),
};

$(document).ready(function () {
  var $html = $('html');
  for (name in browsers) {
    let isIt = !!browsers[name];
    // browsers[browser];
    $html.toggleClass(name, isIt);
  }
});

export default browsers;
