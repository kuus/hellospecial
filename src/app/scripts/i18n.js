import api from './api';

/**
 * Get lang code from url
 *
 * @return {String}
 */
function getLangFromUrl () {
  const urlParts = location.href.split('/');
  const langMap = window.i18n;

  for (let i = 0; i < urlParts.length; i++) {
    let urlPart = urlParts[i];
    if (langMap[urlPart] && urlPart !== 'current') {
      return urlPart;
    }
  }
  return 'en'; // default language
}

/**
 * Get i18n string by its id
 *
 * @param  {String}  id
 * @param  {?String} givenLang
 * @param  {?String} fallback
 * @return {String}
 */
export function getI18n (id, givenLang, fallback) {
  const i18n = window.i18n;
  const lang = givenLang || i18n['current'] || getLangFromUrl();
  const langStrings = i18n[lang];
  return langStrings[id] || fallback || '';
}

api['getI18n'] = getI18n;
